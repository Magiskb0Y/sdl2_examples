#ifndef __MACRO_H__
#define __MACRO_H__

#include <iostream>

#define LOG(msg) \
    std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl

#endif
