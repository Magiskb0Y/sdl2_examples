#ifndef __CONFIG_H__
#define __CONFIG_H__

class Config {
    public:
        static char* WINDOW_TITLE;
        static int   SCREEN_WIDTH;
        static int   SCREEN_HEIGHT;
};

#endif
