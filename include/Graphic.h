#ifndef __GRAPHIC_H__
#define __GRAPHIC_H__

#include <SDL.h>
#include <SDL_image.h>

#include "utils.h"
#include "Config.h"

class Graphic {
    public:
        ~Graphic();
        static Graphic* get();

        void Clear();
        void Update();
        void Draw();

    protected:
        Graphic();

    private:
        static Graphic* instance;
        SDL_Window* wind;
        SDL_Renderer* rend;
};

#endif
